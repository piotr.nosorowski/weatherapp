import requests
from pprint import pprint
import os

class weatherApp:
    def __init__(self):
        self.apiKey = '29fa1fc344b7dd4fd912b48b55491cca'
        self.base_url = "http://api.openweathermap.org/data/2.5/weather?"
        
        weatherApp.MakeMenu(self)

    def MakeMenu(self):
        
        os.system('clear')
        
        print("Pick an option from the menu \n")
        print("1. Get weather by City Name")
        print("2. Get weather by City ID")
        print("3. Get weather by Geographic Coordinates")
        print("4. debug")

        choice = int(input("\n"))

        if choice == 1:
            weatherApp.GetWeatherByName(self)
        elif choice == 2:
            weatherApp.GetWeatherByCityID(self)
        elif choice == 3:
            weatherApp.GetWeatherByCoordinates(self)
        elif choice == 4:
            weatherApp.debug(self)

    def GetWeatherByName(self):                

        self.city_name = str(input("\nName: "))

        self.final_url = self.base_url + "appid="  + self.apiKey + "&q=" + self.city_name + "&units=metric"
        self.weather_data = requests.get(self.final_url).json()
        self.request = self.weather_data

        if self.request['cod'] == '404' or self.request['cod'] == '400':
            print(self.request['cod'])
            print("This doesn't seem right, try again\n")
            input("Press return to continue")
            return weatherApp.MakeMenu(self)   

        self.temperature = self.request['main']
        temperature = int(self.temperature['temp'])
        self.weather = self.request['weather']
        self.description = self.weather[0]
        self.sys = self.request['sys']
        self.country = self.sys['country']

        print('\nWeather for ' + self.request['name'] + ", " + self.country)
        print("Temperature is " + str(temperature) + "°C")
        print("Weather forcast is " + self.description['main'] + "\n")

    def GetWeatherByCityID(self):

        self.cityID = str(input("\nCityID: "))
        
        #Lisbon standard
        #self.cityID = str('2267057')

        self.final_url = self.base_url + "appid="  + self.apiKey + "&id=" + self.cityID + "&units=metric"
        self.weather_data = requests.get(self.final_url).json()
        self.request = self.weather_data
        
        if self.request['cod'] == '404' or self.request['cod'] == '400':
            print(self.request['cod'])
            print("This doesn't seem right, try again\n")
            input("Press return to continue")
            return weatherApp.MakeMenu(self)   

        self.temperature = self.request['main']
        temperature = int(self.temperature['temp'])
        self.weather = self.request['weather']
        self.description = self.weather[0]
        self.sys = self.request['sys']
        self.country = self.sys['country']

        print('\nWeather for ' + self.request['name'] + ", " + self.country)
        print("Temperature is " + str(temperature) + "°C")
        print("Weather forcast is " + self.description['main'] + "\n")

    def GetWeatherByCoordinates(self):


        self.cityLatitude = str(input("\nLatitude: "))
        self.cityLongitude = str(input("Longitude: "))
        
        #api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}

        self.final_url = self.base_url + "appid="  + self.apiKey + "&lat=" + self.cityLatitude + "&lon=" + self.cityLongitude + "&units=metric"
        self.weather_data = requests.get(self.final_url).json()
        self.request = self.weather_data
        
        if self.request['cod'] == '404' or self.request['cod'] == '400':
            print(self.request['cod'])
            print("This doesn't seem right, try again\n")
            input("Press return to continue")
            return weatherApp.MakeMenu(self)   
        
        print(self.request)

        self.temperature = self.request['main']
        temperature = int(self.temperature['temp'])
        self.weather = self.request['weather']
        self.description = self.weather[0]

        try:
            self.sys = self.request['sys']
            self.country = self.sys['country']
        except:
            self.country = ''


        print('\nWeather for ' + self.request['name'] + ", " + self.country)
        print("Temperature is " + str(temperature) + "°C")
        print("Weather forcast is " + self.description['main'] + "\n")


    def debug(self):
        
        self.city_name = "Lisbon"
        self.final_url = self.base_url + "appid="  + self.apiKey + "&q=" + self.city_name + "&units=metric"
        self.weather_data = requests.get(self.final_url).json()
        self.request = self.weather_data
        
        print(self.city_name)
        pprint(self.request)

weatherApp()
